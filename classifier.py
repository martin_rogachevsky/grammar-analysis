
class Classifier:

    def __init__(self, sequence):
        self.terms = {"a","b","c","d","e","f","g","h"}
        self.nonTerms = {"S","A","B","C"}
        self.rules = [("S",("a","A")) ,("A",("b", "B")), ("B",("c", "C", "h")),("C",("d","C","f","g")), ("C",("e"))]

        self.sequence = sequence
        self.valid = True
        for symbol in sequence:
            if not symbol in self.terms:
                self.valid = False
        self.belongToGramm = self.classify(sequence,"S") == len(sequence)

    def classify(self, sequence, nonTerm):
        rules = [rule for rule in self.rules if rule[0] == nonTerm]
        for rule in rules:
            index = 0
            suits = True
            for element in rule[1]:
                if element in self.terms:
                    if index >= len(sequence) or element != sequence[index]:
                        suits = False
                        break
                    else:
                        index += 1
                if element in self.nonTerms:
                    nonTerminalLen = self.classify(sequence[index:], element)
                    if  nonTerminalLen == 0:
                        suits = False
                        break
                    else:
                        index += nonTerminalLen
            if suits:
                return index
        return 0






