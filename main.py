from classifier import Classifier
from plotBuilder import drawResult
from sys import argv

def main():
    sequence = argv[1]
    classifier = Classifier(sequence)
    if not classifier.valid:
        print("Wrong input sequence")
        return
    if classifier.belongToGramm:
        print("Recognized by grammer!")
    else:
        print("Unrecognizable by grammer!")
                
    drawResult(sequence)

main()