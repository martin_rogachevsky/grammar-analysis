import matplotlib.pyplot as plt
import numpy as np
import math

TERMINALS = {"a": -4, "b" : 3, "c": -2, "d": 5, "e": 4, "f": -3, "g": 2, "h": 2}
STEP = 5

def func(x,coef):
    return coef*x

def drawResult(sequence):
    x = 1
    y = 1   
    for i, term in enumerate(sequence):
        plt.plot([x, x + STEP], [y, y + TERMINALS[term]*STEP] )
        x += STEP
        y += TERMINALS[term]*STEP
    plt.show()
        